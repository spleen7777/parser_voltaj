# -*- coding: utf-8 -*-

import os
import logging.config

BASE_PATH = os.path.dirname(os.path.dirname(__file__))
IMAGES = os.path.join(BASE_PATH, 'images')
LOGS = os.path.join(BASE_PATH, 'logs')

DEBUG = True

OPTIONS = {
    "db": {
        'name': 'vscrap',
        'port': '5432',
        'host': '127.0.0.1',
        'password': 'Aqpl308E',
        'user': 'postgres'
    },
    'stops': [],
    'output_folder': os.path.join(BASE_PATH, 'data')
}

if DEBUG:
    OPTIONS = {
        "db": {
            'name': 'vscrap',
            'port': '5433',
            'host': '127.0.0.1',
            'password': 'Aqpl308E',
            'user': 'postgres'
        },
        'output_folder': os.path.join(BASE_PATH, 'data')
    }

# Pool Executor
MAX_WORKERS = 10
WORKER_TIMEOUT = 3600

# Voltag Scrapper option
BASE_URL = 'https://voltag.ru'
START_F2 = 0
END_F2 = 260
START_P = 1
END_P = 2247

# Loggging
logging.config.fileConfig('config/logging.ini')
logger = logging.getLogger('vscrap')


# Console Color
reset = '\033[0m'  # Reset
black = '\033[0;30m'  # Black
white = '\033[0;37m'  # White
red = '\033[0;91m'  # Red
green = '\033[0;92m'  # Green
yellow = '\033[0;93m'  # Yellow
blue = '\033[0;34m'  # Blue
purple = '\033[0;95m'  # Purple
cyan = '\033[0;96m'  # Cyan


# NLP russian stopwords list
SW = 'nlp/stopwords.ru'


def load_stopwords():
    stops = []
    if os.path.exists(SW):
        with open(SW) as f:
            stops = f.readlines()
    return stops


OPTIONS['stops'] = load_stopwords()


def getoptions(): return OPTIONS
