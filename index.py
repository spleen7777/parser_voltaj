# -*- coding: utf-8 -*-

from collections import OrderedDict

import os
from flask import Flask, jsonify
from models import *
import json

options = getoptions()

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False


def get_products():
    result = []
    session = Session()
    try:
        products = session.query(Product).all()
        if products is not None and len(products) > 0:
            count = 0
            for product in products:
                if count > 10: break
                item = OrderedDict()
                if product is not None:
                    item['product'] = {"id": product.id, "name": product.name, "descr": product.descr}
                    cat = product.category
                    if cat is not None:
                        item['category'] = {"id": cat.id, "cid": cat.cid, "name": cat.name, "title": cat.title}

                    pset = product.productext
                    if pset is not None and len(pset) > 0:
                        item['params'] = pset[0].params['params']
                        item['crosses'] = pset[0].crosses['crosses']
                        item['components'] = pset[0].components['components']
                        item['using'] = pset[0].using

                    pictures = product.pictures
                    if pictures is not None and len(pictures) > 0:
                        p = {
                            'local': pictures[0].local['local'],
                            'external': pictures[0].external['external']
                        }
                        item['pictures'] = p
                    result.append(item)
                count += 1

    except Exception as e:
        print(e)
        session.rollback()
    finally:
        Session.remove()
    return result


def save_to_file(data):
    path = options['output_folder']
    file = os.path.join(path, 'data.txt')
    if not os.path.exists(path):
        os.mkdir(path, mode=0o775)
    if os.path.exists(path) and os.path.isdir(path) and os.access(path, os.W_OK) is True:
        with open(file, 'w') as f:
            f.write(data)
    return file


# -----------------------------------------------
# Routers
# -----------------------------------------------
@app.route('/')
def hello():
    return "Get product list following url &lt;/products&gt;"


@app.route('/products')
def product_list():
    d = get_products()
    data = json.dumps(d)
    file = save_to_file(data)
    return "File sucessfully saved on path: {0}".format(file)


if __name__ == '__main__':
    app.run()
