# -*- coding: utf-8 -*-

from decorators import JsonEncodedDict
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, backref, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.orm.collections import attribute_mapped_collection
from config.config import getoptions

options = getoptions()
Base = declarative_base()


class Category(Base):
    __tablename__ = 'category'

    id = Column(Integer, primary_key=True)
    cid = Column(String(20))
    name = Column(String(256), nullable=False)
    title = Column(String(1024), nullable=True)

    parent_id = Column(Integer, ForeignKey(id))
    children = relationship(
        "Category",
        cascade="all, delete-orphan",
        backref=backref("parent", remote_side=id),
        collection_class=attribute_mapped_collection('name'),
    )

    def __init__(self, name, title, cid, parent=None):
        self.name = name
        self.title = title
        self.cid = cid
        self.parent = parent

    def __repr__(self):
        return "Category(name={0}, id={1}, parent_id={2})" .format(self.name, self.id, self.parent_id)


class Product(Base):
    __tablename__ = 'product'

    id = Column(Integer, primary_key=True)
    name = Column(String(256), nullable=False)
    descr = Column(String(2048))

    category_id = Column(Integer, ForeignKey('category.id'))
    category = relationship(Category, backref=backref('products', uselist=True))

    productext = relationship('ProductExt', backref=backref('product', uselist=False), cascade="all, delete-orphan")
    pictures = relationship('Picture', backref=backref('product', uselist=False), cascade="all, delete-orphan")

    def __init__(self, name, descr, category=None):
        self.name = name
        self.descr = descr
        self.category = category

    def __repr__(self):
        return "Product(name={0}, id={1}, category_id={2})".format(self.name, self.id, self.category_id)


class ProductExt(Base):
    __tablename__ = 'product_ext'

    id = Column(Integer, primary_key=True)
    params = Column(JsonEncodedDict)
    crosses = Column(JsonEncodedDict)
    components = Column(JsonEncodedDict)
    replacements = Column(JsonEncodedDict)
    using = Column(String())

    product_id = Column(Integer, ForeignKey('product.id'))

    def __init__(self, params, crosses, components, replacements, using, product=None):
        self.params = params
        self.crosses = crosses
        self.components = components
        self.replacements = replacements
        self.using = using
        self.product = product

    def __repr__(self):
        return "ProductExt(id={0}, product_id={1})".format(self.id, self.product_id)


class Picture(Base):
    __tablename__ = 'product_picture'

    id = Column(Integer, primary_key=True)
    local = Column(JsonEncodedDict)
    external = Column(JsonEncodedDict)

    product_id = Column(Integer, ForeignKey('product.id'))

    def __init__(self, local, external, product=None):
        self.local = local
        self.external = external
        self.product = product

    def __repr__(self):
        return "Picture(id={0}, product_id={1})".format(self.id, self.product_id)


dbpath = "postgresql://{user}:{passw}@{host}:{port}/{name}".format(
    user=options['db']['user'],
    passw=options['db']['password'],
    host=options['db']['host'],
    port=options['db']['port'],
    name=options['db']['name']
)

Engine = create_engine(dbpath, echo=False)
Base.metadata.bind = Engine
session_factory = sessionmaker(bind=Engine)
Session = scoped_session(session_factory)
#after first execute need to be commented
Base.metadata.create_all()
