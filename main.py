# -*- coding: utf-8 -*-

import re
import threading
import datetime
import random
import mechanicalsoup
from bs4 import NavigableString
from config.config import *
from collections import Counter
from time import sleep, time
from urllib.parse import urlsplit
from sqlalchemy.exc import DatabaseError
from concurrent.futures import ThreadPoolExecutor
from models import Session, Category, Product, ProductExt, Picture
from uuid import uuid4

random.seed(datetime.datetime.now())
options = getoptions()


class Main:
    browser = mechanicalsoup.StatefulBrowser()

    @staticmethod
    def split_str_by_endline(string):
        if string is not None and len(string) > 0:
            return [word.replace('\t', ' ').strip() for word in string.split('\n') if word]

    @staticmethod
    def get_must_freq_word(words):
        result = ""
        if words is not None and len(words) > 0:
            ls = [str(word).lower() for word in words if word not in options['stops']]
            r = Counter(ls).most_common(1)
            if r is not None and len(r) > 0:
                key, count = r[0]
                result = key
        return result

    @staticmethod
    def extract_table_params(item):
        result = []
        try:
            if item is not None:
                tbl_div = item.find("div", class_="catalog_group_params")
                if tbl_div is not None:
                    tbl = tbl_div.find('table')
                    if tbl is not None:
                        trows = tbl.find_all('tr')
                        if trows is not None and len(trows) > 0:
                            for row in trows:
                                tds = row.find_all('td')
                                if tds is not None and len(tds) > 0:
                                    prs = {
                                        'lable': len(tds) > 0 and tds[0].get_text().replace('\xa0', ' ') or "",
                                        'descr': len(tds) > 1 and tds[1].get_text().replace('\xa0', ' ') or "",
                                        'value': len(tds) > 2 and tds[2].get_text().replace('\xa0', ' ') or ""
                                    }
                                    result.append(prs)

        except AttributeError as e:
            logger.error(e)

        return result

    @staticmethod
    def extract_table_crosses(item):
        result = []
        try:
            if item is not None:
                tbl_div = item.find("div", class_="catalog_group_crosslist")
                if tbl_div is not None:
                    tbl = tbl_div.find('table')
                    if tbl is not None:
                        trows = tbl.find_all('tr')
                        if trows is not None and len(trows) > 0:
                            for row in trows:
                                tds = row.find_all('td')
                                if tds is not None and len(tds) > 0:
                                    el = {'brand': len(tds) > 0 and tds[0].get_text().replace('\xa0', ' ') or ""}
                                    vendor = len(tds) > 1 and tds[1].get_text().replace('\xa0', ' ') or None
                                    if vendor is not None and vendor:
                                        vendors = re.split(',[\s]+|[\s]+', vendor)
                                        el['vnd'] = vendors

                                    result.append(el)

        except AttributeError as e:
            logger.error(e)

        return result

    @staticmethod
    def extract_table_components(item):
        result = []
        try:
            if item is not None:
                cmp = item.find("div", class_="catalog_group_components_info")
                if cmp is not None:
                    groups = cmp.find_all("div", class_="catalog_group_components_wrap")
                    if groups is not None and len(groups) > 0:
                        for group in groups:
                            el = {}
                            comp_a = group.find("div", class_="catalog_group_components_a")
                            if comp_a is not None:
                                el['brand'] = comp_a.find('span').get_text().replace('\xa0', ' ').strip()
                            vss = group.find_all('div', class_='catalog_group_components_c')
                            if vss is not None and len(vss) > 0:
                                vss_l = []
                                for vs in vss:
                                    vs_el = {}
                                    a = vs.find('div', class_='catalog_group_components_c_n').find('a')
                                    d = vs.find('div', class_='catalog_group_components_c_d')
                                    if a is not None:
                                        vs_el['title'] = a.get_text().replace('\xa0', ' ').strip()
                                    if d is not None:
                                        dsc = d.get_text().replace('\xa0', ' ').strip()
                                        vs_el['descr'] = dsc
                                    vss_l.append(vs_el)
                                el['vnd'] = vss_l
                            result.append(el)

        except AttributeError as e:
            logger.error(e)

        return result

    @staticmethod
    def extract_table_replacements(item):
        result = []
        try:
            if item is not None:
                cgp = item.find("div", class_="catalog_group_replacement_info")
                if cgp is not None:
                    groups = cgp.find_all("div", class_="catalog_group_replacement_item")
                    if groups is not None and len(groups) > 0:
                        for group in groups:
                            a = group.find("a")
                            if a is not None:
                                el = {
                                    'name': a.get_text().replace('\xa0', ' ').strip(),
                                    'link': a['href']
                                }
                                result.append(el)

        except AttributeError as e:
            logger.error(e)

        return result

    @staticmethod
    def extract_images(item):
        result = {}
        if item is not None:
            try:
                photos_div = item.find("div", class_="catalog_group_photos")
                if photos_div is not None:
                    photos = photos_div.find_all("div", class_="catalog_group_photo")
                    if photos is not None and len(photos) > 0:
                        extarnal = []
                        local = []
                        for photo in photos:
                            a = photo.find('a')
                            if a is not None:
                                img = a.find('img')
                                if img is not None:
                                    src = img['src']
                                    if src is not None and len(src) > 0:
                                        extarnal.append(src)
                                        local.append(Main.save_image(src))
                        result['local'] = local
                        result['external'] = extarnal
            except Exception as e:
                logger.error(e)
        return result

    @staticmethod
    def save_image(url):
        result = ""
        if url is not None and url:
            suffix_list = ['jpg', 'gif', 'png', 'tif', 'svg']
            file_name = urlsplit(url)[2].split('/')[-1]
            # file_suffix = file_name.split('.')[1]
            # r = requests.get(url)
            # if file_suffix in suffix_list and r.status_code == requests.codes.ok:
            #     path = os.path.join(IMAGES, file_name)
            #     with iopen(path, 'wb') as file:
            #         file.write(r.content)
            #         result = file_name
        return file_name

    @staticmethod
    def extract_usingfor(item):
        result = ""
        if item is not None:
            try:
                div = item.find("div", class_='catalog_group_application_info')
                if div is not None:
                    ap = div.get_text().replace('\xa0', ' ')
                    result = re.sub('[\s]+', ' ', ap)
            except AttributeError as e:
                logger.error(e)

        return result

    @staticmethod
    def extract_title(item):
        result = ""
        if item is not None:
            title_tag = item.find("div", class_="catalog_group_title")
            if title_tag is not None:
                result = re.sub("new", '', title_tag.get_text(), flags=re.I).strip()
        return result

    @staticmethod
    def extract_descr(item):
        result = ""
        if item is not None:
            title_tag = item.find("div", class_="header_nav")
            if title_tag is not None:
                h1 = title_tag.find("h1")
                if h1 is not None:
                    for child in h1:
                        if isinstance(child, NavigableString):
                            result = child.replace('\xa0', ' ').strip()
        return result

    @staticmethod
    def extract_rows(url):
        result = []
        if url is not None and url:
            try:
                Main.browser.open(url)
                html = Main.browser.get_current_page()
                if html is not None:
                    rows = html.find_all("div", class_="catalog_item")
                    if rows is not None and len(rows) > 0:
                        result = rows
            except (ConnectionError, AttributeError) as e:
                logger.error(e)
            finally:
                pass
        return result

    @staticmethod
    def get_item(url):
        result = {}
        if url is not None and url:
            Main.browser.open(url)
            try:
                html = Main.browser.get_current_page()
                if html is not None:
                    try:
                        item = html.find("div", id="center")
                        if item is not None:
                            result = {
                                'title': Main.extract_title(item),
                                'descr': Main.extract_descr(item),
                                'params': Main.extract_table_params(item),
                                'crosses': Main.extract_table_crosses(item),
                                'components': Main.extract_table_components(item),
                                'replacements': Main.extract_table_replacements(item),
                                'using': Main.extract_usingfor(item),
                                'images': Main.extract_images(item)
                            }
                    except AttributeError as e:
                        logger.error(e)
            except Exception as e:
                logger.error(e)
            finally:
                pass
        return result

    @staticmethod
    def get_items(url, timeout):
        result = {}
        items = []
        fwords = []
        rows = Main.extract_rows(url)
        try:
            if rows is not None and len(rows) > 0:
                cnt = 1
                for row in rows:
                    a = row.find("div", class_="catalog_item_title").find("a")
                    t = row.find("div", class_="catalog_item_subtitle").get_text().replace('\xa0', ' ')
                    fwords.append(str(t).lower())
                    if a is not None and a:
                        url_ = BASE_URL + a['href']
                        if url_:
                            item = Main.get_item(url_)
                            if item is not None:
                                items.append(item)

                        logger.info('{0} [{1}] scrapping url: {2}{3}'.format(yellow, threading.current_thread().name, url_, reset))
                    cnt += 1
            result['items'] = items
            result['category'] = Main.get_must_freq_word(fwords)
        except Exception as e:
            logger.error(e)
        return result

    @staticmethod
    def save_intodb(data):
        if data is not None:
            session = Session()
            try:
                cat_name = data['category']
                items = data['items']
                cat = session.query(Category).filter_by(name=cat_name).first()
                if cat is None:
                    cid = uuid4().hex[:8]
                    cat = Category(name=cat_name, title=cat_name, cid=cid)
                    session.add(cat)

                for item in items:
                    title = item['title']
                    descr = item['descr']
                    params = {'params': item['params']}
                    crosses = {"crosses": item['crosses']}
                    components = {"components": item['components']}
                    replacements = {"replacements": item['replacements']}
                    using = item['using']
                    local = {"local": item['images']['local']}
                    external = {"external": item['images']['external']}

                    product = Product(title, descr, category=cat)
                    product_ext = ProductExt(params, crosses, components, replacements, using, product=product)
                    picture = Picture(local, external, product=product)

                    session.add(product)
                    session.add(product_ext)
                    session.add(picture)

                    session.commit()
                    logger.info('{0} [{1}] save into db: {2}{3}'.format(yellow, threading.current_thread().name, 'product-' + title, reset))

            except DatabaseError as e:
                logger.error(e)
                Session.rollback()
            finally:
                Session.close()

    @staticmethod
    def process(url, timeout):
        try:
            logger.info('{0} start thread: {1}{2}'.format(cyan, threading.current_thread().name, reset))
            items = Main.get_items(url, timeout)
            if items is not None:
                Main.save_intodb(items)
                sleep(10)
        except Exception as e:
            logger.error(e)
        finally:
            pass


if __name__ == '__main__':
    start_t = time()
    try:
        logger.info("{0}{1}{2}".format(purple, 'START SCRAPPING...', reset))
        with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
            for x in range(1, 2258):
                url = BASE_URL + "/catalog/list/p-{0}/".format(x)
                executor.submit(Main.process, url, WORKER_TIMEOUT)
    except Exception as e:
        logger.error(e)
    finally:
        Main.browser.close()
        Session.remove()
        end_t = time() - start_t
        print('Execution time: {0}sec.'.format(end_t))
        print('{0}{1}{2}'.format(purple, 'END SCRAPPING', reset))
