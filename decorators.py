# -*- coding: utf-8 -*-
from sqlalchemy import TypeDecorator, Text

__author__ = 'efanchik'

import json
from sqlalchemy.ext import mutable


class JsonEncodedDict(TypeDecorator):
    """Enables JSON storage by encoding and decoding on the fly."""

    def process_literal_param(self, value, dialect):
        pass

    impl = Text

    def process_bind_param(self, value, dialect):
        if value is None:
            return '{}'
        else:
            return json.dumps(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return {}
        else:
            return json.loads(value)


mutable.MutableDict.associate_with(JsonEncodedDict)
